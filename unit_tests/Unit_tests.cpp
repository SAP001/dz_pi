﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <math.h>
#include <ctype.h>
#include <assert.h>
#include <vector>
#include <algorithm>
#include "../src/logic.cpp"

#define MAX_STR 5
#define MAX_LENGTH 1000
#define EMPTY_FILE -1
#define NONEXISTENT_FILE -2
#define INCORRECT_DATA -3
#define EXCESS_OF_LINES -4

using namespace std;

void test_toArray(void)
{
	int err_cnt = 0;
	int rc = 0;
	vector<string> words;
	vector<int> figures;
	int k_words = 0;
	int k_figures = 0;
	//Некорректные данные
	//vector<string>& words, vector<int>& figures, int* rc, int* k_words, int* k_figures, char* str
	{	
		char str1[MAX_LENGTH] = "-8 fact";
		toArray(words, figures, &rc, &k_words, &k_figures, str1);
		if (rc != INCORRECT_DATA)
		{
			err_cnt++;
		}

		rc = 0;
		char str2[MAX_LENGTH] = "soda, hot ! ";
		toArray(words, figures, &rc, &k_words, &k_figures, str2);
		if (rc != INCORRECT_DATA)
		{
			err_cnt++;
		}

		rc = 0;
		char str3[MAX_LENGTH] = "Apple 5among";
		toArray(words, figures, &rc, &k_words, &k_figures, str3);
		if (rc != INCORRECT_DATA)
		{
			err_cnt++;
		}
	}
	//Корректные данные
	{
		rc = 0;
		char str4[MAX_LENGTH] = "235 ahdjfs fws";
		toArray(words, figures, &rc, &k_words, &k_figures, str4);
		if (rc != 0)
		{
			err_cnt++;
		}

		rc = 0;
		char str5[MAX_LENGTH] = "2 ahdjfs 35 fws";
		toArray(words, figures, &rc, &k_words, &k_figures, str5);
		if (rc != 0)
		{
			err_cnt++;
		}

		rc = 0;
		char str6[MAX_LENGTH] = "fws 2 ahdjfs 35";
		toArray(words, figures, &rc, &k_words, &k_figures, str6);
		if (rc != 0)
		{
			err_cnt++;
		}

		rc = 0;
		char str7[MAX_LENGTH] = "fws 2 3 5 ahdjfs";
		toArray(words, figures, &rc, &k_words, &k_figures, str7);
		if (rc != 0)
		{
			err_cnt++;
		}

		rc = 0;
		char str8[MAX_LENGTH] = "2 4";
		toArray(words, figures, &rc, &k_words, &k_figures, str8);
		if (rc != 0)
		{
			err_cnt++;
		}

		rc = 0;
		char str9[MAX_LENGTH] = "ahdjfs fws";
		toArray(words, figures, &rc, &k_words, &k_figures, str9);
		if (rc != 0)
		{
			err_cnt++;
		}

		rc = 0;
		char str10[MAX_LENGTH] = "fws ahdjfs";
		toArray(words, figures, &rc, &k_words, &k_figures, str10);
		if (rc != 0)
		{
			err_cnt++;
		}
	}
	printf("%s: %s\n", __func__, err_cnt ? "FAILED" : "OK");
}

void test_sortWords(void)
{
	int err_cnt = 0;
	vector<string> words_sort = { "ela", "kashu", "masha" };
	//Корректные данные
	{
		vector<string> words1 = { "masha", "ela", "kashu" };
		sortWords(words1);
		if (words1 != words_sort)
		{
			err_cnt++;
		}

		vector<string> words2 = { "ela", "kashu", "masha" };
		sortWords(words2);
		if (words2 != words_sort)
		{
			err_cnt++;
		}

		vector<string> words_sort3 = { "and", "ela", "kashu", "mandarin", "masha" };
		vector<string> words3 = { "masha", "kashu", "ela", "and", "mandarin" };
		sortWords(words3);
		if (words3 != words_sort3)
		{
			err_cnt++;
		}

		vector<string> words4 = { "ela" };
		vector<string> words_sort1 = { "ela" };
		sortWords(words4);
		if (words4 != words_sort1)
		{
			err_cnt++;
		}
	}
	printf("%s: %s\n", __func__, err_cnt ? "FAILED" : "OK");
}

void test_sumFigures(void)
{
	int err_cnt = 0;
	//Корректные данные
	{
		vector<int> figures1 = { 2, 4, 5 };
		int k_figures1 = 3;
		if (sumFigures(figures1, &k_figures1) != 11)
		{
			err_cnt++;
		}

		vector<int> figures2 = {};
		int k_figures2 = 0;
		if (sumFigures(figures2, &k_figures2) != -1)
		{
			err_cnt++;
		}
	}
	printf("%s: %s\n", __func__, err_cnt ? "FAILED" : "OK");
}

void test_joinString(void)
{
	int err_cnt = 0;
	//Корректные данные
	{
	//int* sum, vector<string>& words, int* k_words
		int sum1 = 11;
		vector<string> words1 = { "ahdjfs", "fws" };
		int k_words1 = 2;
		if (joinString(&sum1, words1, &k_words1) != "ahdjfs fws 11")
		{
			err_cnt++;
		}

		int sum2 = -1;
		vector<string> words2 = { "ahdjfs", "fws" };
		int k_words2 = 2;
		if (joinString(&sum2, words2, &k_words2) != "ahdjfs fws ")
		{
			err_cnt++;
		}

		int sum3 = 11;
		vector<string> words3 = {};
		int k_words3 = 0;
		if (joinString(&sum3, words3, &k_words3) != "11")
		{
			err_cnt++;
		}
	}
	printf("%s: %s\n", __func__, err_cnt ? "FAILED" : "OK");
}

void test_conversionString(void)
{
	int err_cnt = 0;
	int rc = 0;
	//Корректные данные
	{
		char str1[MAX_LENGTH] = "235 ahdjfs fws";
		if (conversionString(str1, &rc) != "ahdjfs fws 10")
		{
			err_cnt++;
		}

		char str2[MAX_LENGTH] = "2 ahdjfs 35 fws";
		if (conversionString(str2, &rc) != "ahdjfs fws 10")
		{
			err_cnt++;
		}

		char str3[MAX_LENGTH] = "fws 2 ahdjfs 35";
		if (conversionString(str3, &rc) != "ahdjfs fws 10")
		{
			err_cnt++;
		}

		char str4[MAX_LENGTH] = "fws 2 3 5 ahdjfs ";
		if (conversionString(str4, &rc) != "ahdjfs fws 10")
		{
			err_cnt++;
		}

		char str5[MAX_LENGTH] = "2 4";
		if (conversionString(str5, &rc) != "6")
		{
			err_cnt++;
		}

		char str6[MAX_LENGTH] = "ahdjfs fws";
		if (conversionString(str6, &rc) != "ahdjfs fws ")
		{
			err_cnt++;
		}

		char str7[MAX_LENGTH] = "fws ahdjfs";
		if (conversionString(str7, &rc) != "ahdjfs fws ")
		{
			err_cnt++;
		}
	}
	printf("%s: %s\n", __func__, err_cnt ? "FAILED" : "OK");
}

int main()
{
	setlocale(LC_ALL, "Russian");
	test_toArray();
	test_sortWords();
	test_sumFigures();
	test_joinString();
	test_conversionString();
	return 0;
}