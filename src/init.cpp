#include "logic.cpp"
int main( int argc, char * argv[] )
{
    setlocale(LC_ALL, "Russian");
    int status = 0;
    int i = 0;
    char filename[MAX_LENGTH];
    char new_filename[MAX_LENGTH];
    strcpy(filename, argv[1]);
    strcpy(new_filename, argv[2]);
	ifstream file(filename);
    ofstream new_file(new_filename);
    char str[MAX_LENGTH];
	if (file.is_open() && new_file.is_open() ) 
    {
		if (file.peek() != EOF)
		{
			while (!file.eof() && status == 0)
			{
                readFile(file, &status, str);
                if (status == 0)
                {
                    string new_str = conversionString(str, &status);
                    if (status == 0)
                    {
                        writingToFile(new_str, new_file);
                    }
                }
			}
		}
		else
		{
			status = EMPTY_FILE;
		}
		new_file.close();
		file.close();	
	}
    else
    {
        status = NONEXISTENT_FILE;
    }
    if (status == EMPTY_FILE)
    {
        printf("File is empty\n");
    }
    if (status == NONEXISTENT_FILE)
    {
        printf("Non-exist file\n");
    }
    if (status == INCORRECT_DATA)
    {
        printf("Incorrect data\n");
    }
    if (status == EXCESS_OF_LINES)
    {
        printf("The number of lines is more than 5\n");
    }
    if (status == 0)
    {
        printf("OK!\n");
    }
    return status;
}