#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <math.h>
#include <ctype.h>
#include <assert.h>
#include <vector>
#include <algorithm>

#define MAX_STR 5
#define MAX_LENGTH 1000
#define EMPTY_FILE -1
#define NONEXISTENT_FILE -2
#define INCORRECT_DATA -3
#define EXCESS_OF_LINES -4

using namespace std;


void readFile(ifstream& file, int* status, char* str) 
{
	assert(file || file.peek() != EOF);
	file.getline(str, MAX_LENGTH, '\n');
	if (strlen(str) == 0)
	{
		*status = INCORRECT_DATA;
	}
}

void toArray(vector<string>& words, vector<int>& figures, int* status, int* k_words, int* k_figures, char* str)
{
	assert(status != 0);
	int i = 0;
	int j = 0;
	int k = 0;
	string word = "";
	while (i <= strlen(str) && *status == 0)
	{
		if (isalpha(str[i]) || isdigit(str[i]) || str[i] == ' ' || str[i] == '\0')
		{
			if (i != 0)
			{
				if (isdigit(str[i]) && isalpha(str[i - 1]) || isdigit(str[i - 1]) && isalpha(str[i]))
				{
					*status = INCORRECT_DATA;
				}
			}
			if (*status == 0)
			{
				if (isdigit(str[i]))
				{
					figures.push_back(str[i] - '0');
					j++;
				}
				if (isalpha(str[i]))
				{
					word = word + str[i];
				}
				if ((str[i] == ' ' || str[i] == '\0') && word != "")
				{
					words.push_back(word);
					k++;
					word = "";
				}
			}
		}
		else
		{
			*status = INCORRECT_DATA;
		}
		i++;
	}
	if (*status == 0)
	{
		*k_words = k;
		*k_figures = j;
	}
}

void sortWords(vector<string>& words)
{
	sort(words.begin(), words.end());
}

int sumFigures(vector<int>& figures, int* k_figures)
{
	int i = 0;
	int sum = -1;
	while (i < *k_figures)
	{
		sum += figures[i];
		i++;
	}
	if (sum != -1)
	{
		sum++;
	}
	return sum;
}

string joinString(int* sum, vector<string>& words, int* k_words)
{
	assert(*sum != -1 || *k_words != 0);
	string new_str = "";
	for (int j = 0; j < *k_words; j++)
	{
		new_str += words[j];
		new_str += ' ';
	}
	if (*sum != -1)
	{
		char figure[100];
        sprintf(figure,"%d", *sum);
		new_str += figure;
	}
	return new_str;
}

string conversionString(char* str, int* status)
{
	assert(status != 0);
	vector<string> words;
	vector<int> figures;
	int k_words = 0;
	int k_figures = 0;
	string new_str = "";
	toArray(words, figures, status, &k_words, &k_figures, str);
	if (*status == 0)
	{
		if (k_words > 1)
		{
			sortWords(words);
		}
		int sum = sumFigures(figures, &k_figures);
		if (sum != -1 || k_words != 0)
		{
			new_str = joinString(&sum, words, &k_words);
		}
		else
		{
			*status = INCORRECT_DATA;
		}
	}
	return new_str;
}

void writingToFile(string new_string, ofstream& new_file)
{
	assert(new_string != "" && new_file);
	new_file << new_string + '\n';
}

